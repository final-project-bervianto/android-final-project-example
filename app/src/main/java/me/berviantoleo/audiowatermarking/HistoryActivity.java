package me.berviantoleo.audiowatermarking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ahamed.multiviewadapter.SimpleRecyclerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.berviantoleo.audiowatermarking.api.WebServiceConnector;
import me.berviantoleo.audiowatermarking.item.HistoryEmbedItem;
import me.berviantoleo.audiowatermarking.item.HistoryExtractItem;
import me.berviantoleo.audiowatermarking.model.EmbedData;
import me.berviantoleo.audiowatermarking.model.ExtractData;

public class HistoryActivity extends AppCompatActivity {

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private FirebaseAuth mAuth;
    private static String restoredText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        SharedPreferences prefs = getSharedPreferences("Audio_Watermarking", MODE_PRIVATE);
        restoredText = prefs.getString("Address", "http://bervita.if.itb.ac.id/api/");
        mAuth = FirebaseAuth.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportBar = getSupportActionBar();
        if (supportBar != null) {
            supportBar.setDisplayHomeAsUpEnabled(true);
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            runLogin();
        }
    }

    private void runLogin() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int section;
        private SimpleRecyclerAdapter<EmbedData, HistoryEmbedItem> itemAdapter;
        private SimpleRecyclerAdapter<ExtractData, HistoryExtractItem> itemAdapter1;
        private HistoryEmbedItem.OnItemClickListener listener = item -> {
            Intent intent = new Intent(getActivity(), DetailEmbedActivity.class);
            String url = item.getUrl();
            String[] el = url.split("/");
            intent.putExtra("id", el[el.length - 1]);
            startActivity(intent);
        };

        private HistoryExtractItem.ItemClickListener listener1 = item -> {
            Intent intent = new Intent(getActivity(), DetailExtractActivity.class);
            String url = item.getUrl();
            String[] el = url.split("/");
            intent.putExtra("id", el[el.length - 1]);
            startActivity(intent);
        };


        public PlaceholderFragment() {
        }

        @BindView(R.id.historyList)
        XRecyclerView historyList;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if (arguments != null) {
                section = arguments.getInt(ARG_SECTION_NUMBER);
            }
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_history, container, false);
            ButterKnife.bind(this, rootView);
            itemAdapter = new SimpleRecyclerAdapter<>(new HistoryEmbedItem(listener));
            itemAdapter1 = new SimpleRecyclerAdapter<>(new HistoryExtractItem(listener1));
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            historyList.setLayoutManager(layoutManager);
            historyList.setLoadingMoreEnabled(false);
            if (section == 0) {
                historyList.setAdapter(itemAdapter);
            } else {
                historyList.setAdapter(itemAdapter1);
            }
            historyList.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    refresh();
                }

                @Override
                public void onLoadMore() {

                }
            });
            return rootView;
        }

        @Override
        public void onStart() {
            super.onStart();
            historyList.refresh();
        }

        private void refresh() {

            if (section == 0) {
                WebServiceConnector.getInstance(restoredText).getEmbed(new WebServiceConnector.ApiCallback<List<EmbedData>>() {
                    @Override
                    public void onSuccess(List<EmbedData> response) {
                        itemAdapter.setData(response);
                        itemAdapter.notifyDataSetChanged();
                        historyList.refreshComplete();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        historyList.refreshComplete();
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (section == 1) {
                WebServiceConnector.getInstance(restoredText).getExtract(new WebServiceConnector.ApiCallback<List<ExtractData>>() {
                    @Override
                    public void onSuccess(List<ExtractData> response) {
                        itemAdapter1.setData(response);
                        itemAdapter1.notifyDataSetChanged();
                        historyList.refreshComplete();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        historyList.refreshComplete();
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "Embed";
            } else if (position == 1) {
                return "Extract";
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
