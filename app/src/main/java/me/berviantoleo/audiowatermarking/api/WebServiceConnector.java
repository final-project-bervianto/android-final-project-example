package me.berviantoleo.audiowatermarking.api;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.util.List;
import java.util.concurrent.TimeUnit;

import me.berviantoleo.audiowatermarking.BaseApplication;
import me.berviantoleo.audiowatermarking.BuildConfig;
import me.berviantoleo.audiowatermarking.model.EmbedData;
import me.berviantoleo.audiowatermarking.model.ExtractData;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebServiceConnector {
    //private static String BASE_URL = "http://ec2-34-211-82-246.us-west-2.compute.amazonaws.com:8000/api/";
    // private static String BASE_URL = "http://103.200.4.7:8000/api/";
    private static String BASE_URL = "http://bervita.if.itb.ac.id/api/";
    private final String TAG = "API";

    private WebServiceInterface mApiServices;
    private Gson mGson;
    private static WebServiceConnector mApiConnector;

    public static WebServiceConnector getInstance(String Url) {
        if (Url != null) {
            mApiConnector = new WebServiceConnector(Url);
        } else {
            if (mApiConnector == null) {
                mApiConnector = new WebServiceConnector(BASE_URL);
            }
        }
        return mApiConnector;
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    public Gson getGson() {
        return mGson;
    }

    private WebServiceConnector(String baseURL) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        builder.addInterceptor(logging);
        if (BuildConfig.DEBUG)
            builder.addInterceptor(new ChuckInterceptor(BaseApplication.getInstance().getApplicationContext()));

        mGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        OkHttpClient client = builder.build();

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .build();
        mApiServices = adapter.create(WebServiceInterface.class);
    }

    public interface ApiCallback<T> {
        void onSuccess(T response);

        void onFailure(Throwable t);
    }

    public void postEmbed(String audioLoc, String imageLoc, String key, String accessToken, final ApiCallback<EmbedData> callback) {
        EmbedData embedData = new EmbedData();
        embedData.setAccessToken(accessToken);
        embedData.setAudio_input(audioLoc);
        embedData.setImage_input(imageLoc);
        embedData.setMethod_option("embed_1");
        embedData.setKey(key);
        Call<EmbedData> loginResponseCall = mApiServices.embedDataPost(embedData);
        loginResponseCall.enqueue(new Callback<EmbedData>() {
            @Override
            public void onResponse(@NonNull Call<EmbedData> call, @NonNull Response<EmbedData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "Embed Failed. Error code : " + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmbedData> call, @NonNull Throwable t) {
                Log.i(TAG, "Embed Failed. Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public void getEmbed(final ApiCallback<List<EmbedData>> callback) {
        Call<List<EmbedData>> embedDatas = mApiServices.embedData();
        embedDatas.enqueue(new Callback<List<EmbedData>>() {
            @Override
            public void onResponse(@NonNull Call<List<EmbedData>> call, @NonNull Response<List<EmbedData>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "FAILED GET EMBED DATA");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<EmbedData>> call, @NonNull Throwable t) {
                Log.i(TAG, "Failed. Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public void getExtract(final ApiCallback<List<ExtractData>> callback) {
        Call<List<ExtractData>> embedDatas = mApiServices.extractData();
        embedDatas.enqueue(new Callback<List<ExtractData>>() {
            @Override
            public void onResponse(@NonNull Call<List<ExtractData>> call, @NonNull Response<List<ExtractData>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "FAILED GET EMBED DATA");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<ExtractData>> call, @NonNull Throwable t) {
                Log.i(TAG, "Failed. Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public void postExtract(String oriAudio, String watermarkAudio, String key, String size, String accesstoken, final ApiCallback<ExtractData> callback) {
        ExtractData data = new ExtractData();
        data.setAccessToken(accesstoken);
        data.setMethod_option("extract_1");
        data.setOriginal_audio_input(oriAudio);
        data.setWatermarked_audio_input(watermarkAudio);
        data.setSize(size);
        data.setKey(key);
        Call<ExtractData> extractDataCall = mApiServices.extractDataPost(data);
        extractDataCall.enqueue(new Callback<ExtractData>() {
            @Override
            public void onResponse(@NonNull Call<ExtractData> call, @NonNull Response<ExtractData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "Embed Failed. Error code : " + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ExtractData> call, @NonNull Throwable t) {
                Log.i(TAG, "Embed Failed. Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public void seeDetailEmbed(String id, final ApiCallback<EmbedData> callback) {
        Call<EmbedData> embedDataCall = mApiServices.detailEmbed(id);
        embedDataCall.enqueue(new Callback<EmbedData>() {
            @Override
            public void onResponse(@NonNull Call<EmbedData> call, @NonNull Response<EmbedData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "Failed GET Embed Data. Error code : " + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmbedData> call, @NonNull Throwable t) {
                Log.i(TAG, "Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public void seeDetailExtract(String id, final ApiCallback<ExtractData> callback) {
        Call<ExtractData> extractDataCall = mApiServices.detailExtract(id);
        extractDataCall.enqueue(new Callback<ExtractData>() {
            @Override
            public void onResponse(@NonNull Call<ExtractData> call, @NonNull Response<ExtractData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    Log.i(TAG, "Failed GET Extract Data. Error code : " + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ExtractData> call, @NonNull Throwable t) {
                Log.i(TAG, "Message : " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

}
