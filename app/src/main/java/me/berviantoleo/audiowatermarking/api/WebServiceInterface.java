package me.berviantoleo.audiowatermarking.api;

import java.util.List;

import me.berviantoleo.audiowatermarking.model.EmbedData;
import me.berviantoleo.audiowatermarking.model.ExtractData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WebServiceInterface {
    @GET("v1/extract/")
    Call<List<ExtractData>> extractData();

    @POST("v1/extract/")
    Call<ExtractData> extractDataPost(@Body ExtractData embedData);

    @GET("v1/embed/")
    Call<List<EmbedData>> embedData();

    @POST("v1/embed/")
    Call<EmbedData> embedDataPost(@Body EmbedData embedData);

    @GET("v1/embed/{id}/")
    Call<EmbedData> detailEmbed(@Path("id") String id);

    @GET("v1/extract/{id}/")
    Call<ExtractData> detailExtract(@Path("id") String id);

}
