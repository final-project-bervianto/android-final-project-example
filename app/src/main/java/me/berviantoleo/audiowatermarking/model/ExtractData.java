package me.berviantoleo.audiowatermarking.model;

public class ExtractData {
    private String url;
    private String status;
    private String method_option;
    private String created_at;
    private String update_at;
    private String watermarked_audio_input;
    private String original_audio_input;
    private String size;
    private String key;
    private String accessToken;
    private String user_id;
    private String image_output;
    private String result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMethod_option() {
        return method_option;
    }

    public void setMethod_option(String method_option) {
        this.method_option = method_option;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getWatermarked_audio_input() {
        return watermarked_audio_input;
    }

    public void setWatermarked_audio_input(String watermarked_audio_input) {
        this.watermarked_audio_input = watermarked_audio_input;
    }

    public String getOriginal_audio_input() {
        return original_audio_input;
    }

    public void setOriginal_audio_input(String original_audio_input) {
        this.original_audio_input = original_audio_input;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage_output() {
        return image_output;
    }

    public void setImage_output(String image_output) {
        this.image_output = image_output;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
