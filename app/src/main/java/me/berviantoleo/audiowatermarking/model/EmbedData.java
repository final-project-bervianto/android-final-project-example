package me.berviantoleo.audiowatermarking.model;

public class EmbedData {
    private String url;
    private String status;
    private String method_option;
    private String created_at;
    private String update_at;
    private String image_input;
    private String audio_input;
    private String key;
    private String accessToken;
    private String user_id;
    private String audio_output;
    private String result;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMethod_option() {
        return method_option;
    }

    public void setMethod_option(String method_option) {
        this.method_option = method_option;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getImage_input() {
        return image_input;
    }

    public void setImage_input(String image_input) {
        this.image_input = image_input;
    }

    public String getAudio_input() {
        return audio_input;
    }

    public void setAudio_input(String audio_input) {
        this.audio_input = audio_input;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAudio_output() {
        return audio_output;
    }

    public void setAudio_output(String audio_output) {
        this.audio_output = audio_output;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
