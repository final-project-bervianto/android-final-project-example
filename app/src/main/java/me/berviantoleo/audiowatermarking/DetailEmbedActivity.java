package me.berviantoleo.audiowatermarking;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;
import me.berviantoleo.audiowatermarking.api.WebServiceConnector;
import me.berviantoleo.audiowatermarking.model.EmbedData;

public class DetailEmbedActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private StorageReference mStorageReference;
    private AlertDialog dialog;
    private String id;

    @BindView(R.id.embed_status_detail)
    TextView status;

    @BindView(R.id.start_at_embed)
    TextView startAt;

    @BindView(R.id.update_at_embed)
    TextView updateAt;

    @BindView(R.id.embed_image)
    ImageView embedImage;

    @BindView(R.id.embed_audio_path)
    TextView embedAudioPath;

    @BindView(R.id.embed_watermarked_audio_path)
    TextView watermarkedPath;
    private String image;
    private String audioOutput;
    private String audioInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_embed);
        ButterKnife.bind(this);
        dialog = new SpotsDialog.Builder().setContext(this).build();
        mAuth = FirebaseAuth.getInstance();
        mStorageReference = FirebaseStorage.getInstance().getReference();
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            runLogin();
        }
        dialog.show();
        SharedPreferences prefs = getSharedPreferences("Audio_Watermarking", MODE_PRIVATE);
        String restoredText = prefs.getString("Address", "http://bervita.if.itb.ac.id/api/");
        WebServiceConnector.getInstance(restoredText).seeDetailEmbed(id, new WebServiceConnector.ApiCallback<EmbedData>() {
            @Override
            public void onSuccess(EmbedData response) {
                dialog.dismiss();
                if (response != null) {
                    status.setText(String.format("Status: %s", response.getStatus()));
                    startAt.setText(String.format("Start at: %s", response.getCreated_at()));
                    updateAt.setText(String.format("Update at: %s", response.getUpdate_at()));
                    audioInput = response.getAudio_input();
                    audioOutput = response.getAudio_output();
                    embedAudioPath.setText(String.format("Ori Audio Path: %s", response.getAudio_input()));
                    watermarkedPath.setText(String.format("Watermarked Audio Path: %s", response.getAudio_output()));
                    image = response.getImage_input();
                    updateImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Get DATA", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failed Get DATA", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void runLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateImage() {
        StorageReference storageReference = mStorageReference.child(image);
        GlideApp.with(this)
                .load(storageReference)
                .into(embedImage);
    }

    @OnClick(R.id.download_embed_audio)
    void downloadAudioEmbed() {
        if (audioInput != null) {
            dialog.show();
            StorageReference storageReference = mStorageReference.child(audioInput);
            File external = Environment.getExternalStorageDirectory();
            File output = new File(external, "/audiowatermark/");
            if (!output.exists()) {
                output.mkdir();
            }
            File location = new File(output, String.format("/ori-%s.wav", System.currentTimeMillis()));
            storageReference.getFile(location).addOnSuccessListener(taskSnapshot -> {
                dialog.hide();
                Toast.makeText(getApplicationContext(), "FINISH", Toast.LENGTH_SHORT).show();
            }).addOnFailureListener(e -> Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show());
        } else {
            Toast.makeText(getApplicationContext(), "Can't Download", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.download_embed_watermarked_audio)
    void downloadAudioOutput() {
        if (audioOutput != null) {
            dialog.show();
            StorageReference storageReference = mStorageReference.child(audioOutput);
            File external = Environment.getExternalStorageDirectory();
            File output = new File(external, "/audiowatermark/");
            if (!output.exists()) {
                output.mkdir();
            }
            File location = new File(output, String.format("/watermarked-%s.wav", System.currentTimeMillis()));
            storageReference.getFile(location).addOnSuccessListener(taskSnapshot -> {
                dialog.hide();
                Toast.makeText(getApplicationContext(), "FINISH", Toast.LENGTH_SHORT).show();
            }).addOnFailureListener(e -> Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show());
        } else {
            Toast.makeText(getApplicationContext(), "Can't Download", Toast.LENGTH_SHORT).show();
        }
    }
}
