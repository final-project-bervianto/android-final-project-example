package me.berviantoleo.audiowatermarking;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.folderselector.FileChooserDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;
import me.berviantoleo.audiowatermarking.api.WebServiceConnector;
import me.berviantoleo.audiowatermarking.model.ExtractData;

public class ExtractActivity extends AppCompatActivity implements FileChooserDialog.FileCallback {

    private static final int ORI_CODE = 200;
    private static final int WATER_CODE = 100;
    private StorageReference mStorageReference;
    private FirebaseAuth mAuth;

    @BindView(R.id.extract_watermark_path)
    EditText watermarkPath;

    @BindView(R.id.ori_audio_path)
    EditText oriPath;

    @BindView(R.id.keyExtract)
    EditText keyText;

    @BindView(R.id.imageSizeText)
    EditText imageSize;

    String oriFix;
    boolean oriReady = false;
    String waterFix;
    boolean waterReady = false;
    private AlertDialog dialog;
    private String key;
    private String size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extract);
        ButterKnife.bind(this);
        dialog = new SpotsDialog.Builder().setContext(this).build();
        mAuth = FirebaseAuth.getInstance();
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            runLogin();
        }
    }

    private void runLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.uploadExtract)
    void uploadEmbed() {
        String ori = oriPath.getText().toString();
        String water = watermarkPath.getText().toString();
        String key = keyText.getText().toString();
        String size = imageSize.getText().toString();
        if (ori.equalsIgnoreCase("") && water.equalsIgnoreCase("")
                && key.equalsIgnoreCase("") && size.equalsIgnoreCase("")) {
            Toast.makeText(this, "Filled All Field", Toast.LENGTH_SHORT).show();
        } else {
            uploadData(ori, water, key, size);
        }
    }

    private void uploadData(final String ori, final String water, final String key, final String size) {
        if (!oriReady) {
            dialog.show();
            File audioFile = new File(ori);
            String audioName = audioFile.getName();
            Uri fileAudio = Uri.fromFile(audioFile);
            String target = String.format("%s/extract/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), audioName);
            oriFix = target;
            this.key = key;
            this.size = size;
            StorageReference audioRef = mStorageReference.child(target);
            audioRef.putFile(fileAudio)
                    .addOnSuccessListener(taskSnapshot -> {
                        oriReady = true;
                        if (!waterReady) {
                            File pictFile = new File(water);
                            String pictName = pictFile.getName();
                            Uri filePict = Uri.fromFile(pictFile);
                            String targetPict = String.format("%s/extract/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), pictName);
                            waterFix = targetPict;
                            StorageReference pictRef = mStorageReference.child(targetPict);
                            pictRef.putFile(filePict)
                                    .addOnSuccessListener(taskSnapshot1 -> {
                                        dialog.dismiss();
                                        waterReady = true;
                                        uploadToServer();
                                    }).addOnFailureListener(e -> {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            });
                        }
                    }).addOnFailureListener(e -> {
                dialog.hide();
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            });
        } else {
            if (!waterReady) {
                File pictFile = new File(water);
                String pictName = pictFile.getName();
                Uri filePict = Uri.fromFile(pictFile);
                String targetPict = String.format("%s/extract/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), pictName);
                waterFix = targetPict;
                StorageReference pictRef = mStorageReference.child(targetPict);
                pictRef.putFile(filePict)
                        .addOnSuccessListener(taskSnapshot -> {
                            dialog.dismiss();
                            waterReady = true;
                            uploadToServer();
                        }).addOnFailureListener(e -> {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                });
            }
        }
    }

    private void uploadToServer() {
        dialog.show();
        mAuth.getCurrentUser().getIdToken(true)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String idTok = task.getResult().getToken();
                        SharedPreferences prefs = getSharedPreferences("Audio_Watermarking", MODE_PRIVATE);
                        String restoredText = prefs.getString("Address", "http://bervita.if.itb.ac.id/api/");
                        WebServiceConnector.getInstance(restoredText).postExtract(oriFix, waterFix, key, size, idTok,
                                new WebServiceConnector.ApiCallback<ExtractData>() {
                                    @Override
                                    public void onSuccess(ExtractData response) {
                                        dialog.dismiss();
                                        if (response != null) {
                                            Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "FAILED", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.select_ori_audio)
    void selectAudio() {
        new FileChooserDialog.Builder(this)
                .extensionsFilter(".wav") // Optional extension filter, will override mimeType()
                .tag("original")
                .show(this);
    }

    @OnClick(R.id.select_extract)
    void selectExtract() {
        new FileChooserDialog.Builder(this)
                .extensionsFilter(".wav") // Optional extension filter, will override mimeType()
                .tag("watermarked")
                .show(this);
    }

    @Override
    public void onFileSelection(@NonNull FileChooserDialog dialog, @NonNull File file) {
        final String tag = dialog.getTag();
        if (tag != null) {
            if (tag.equalsIgnoreCase("watermarked") || tag.equalsIgnoreCase("original")) {
                String path = file.getPath();
                if (tag.equalsIgnoreCase("watermarked")) {
                    watermarkPath.setText(path);
                } else {
                    oriPath.setText(path);
                }
            } else {
                Toast.makeText(this, "Failed Pick Audio", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Failed Pick Audio", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFileChooserDismissed(@NonNull FileChooserDialog dialog) {

    }
}
