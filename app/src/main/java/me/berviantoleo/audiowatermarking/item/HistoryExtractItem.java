package me.berviantoleo.audiowatermarking.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahamed.multiviewadapter.ItemBinder;
import com.ahamed.multiviewadapter.ItemViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.berviantoleo.audiowatermarking.R;
import me.berviantoleo.audiowatermarking.model.ExtractData;

public class HistoryExtractItem extends ItemBinder<ExtractData, HistoryExtractItem.ViewHolder> {

    public interface ItemClickListener {
        void click(ExtractData data);
    }

    private static ItemClickListener listener;

    public HistoryExtractItem(ItemClickListener listener1) {
        listener = listener1;
    }

    @Override
    public ViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        return new HistoryExtractItem.ViewHolder(inflater.inflate(R.layout.item_extract_history, parent, false));
    }

    @Override
    public void bind(ViewHolder holder, ExtractData item) {
        holder.audioOriginal.setText(item.getOriginal_audio_input());
        holder.audioWatermarked.setText(item.getWatermarked_audio_input());
        holder.timeStart.setText(item.getCreated_at());
        holder.historyStatus.setText(item.getStatus());
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof ExtractData;
    }

    private static void click(ExtractData data) {
        listener.click(data);
    }

    public static class ViewHolder extends ItemViewHolder<ExtractData> {
        @BindView(R.id.history_audio_original)
        TextView audioOriginal;
        @BindView(R.id.history_audio_watermarked)
        TextView audioWatermarked;
        @BindView(R.id.history_time_start_extract)
        TextView timeStart;
        @BindView(R.id.history_status_extract)
        TextView historyStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            setItemClickListener((view1, item) -> click(item));
        }
    }
}
