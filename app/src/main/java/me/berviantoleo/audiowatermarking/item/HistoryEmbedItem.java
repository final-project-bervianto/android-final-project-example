package me.berviantoleo.audiowatermarking.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahamed.multiviewadapter.ItemBinder;
import com.ahamed.multiviewadapter.ItemViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.berviantoleo.audiowatermarking.R;
import me.berviantoleo.audiowatermarking.model.EmbedData;

public class HistoryEmbedItem extends ItemBinder<EmbedData, HistoryEmbedItem.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(EmbedData item);
    }

    private static OnItemClickListener listener;

    public HistoryEmbedItem(OnItemClickListener itemClickListener) {
        listener = itemClickListener;
    }

    @Override
    public ViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        return new HistoryEmbedItem.ViewHolder(inflater.inflate(R.layout.item_embed_history, parent, false));
    }

    private static void click(EmbedData data) {
        listener.onItemClick(data);
    }

    @Override
    public void bind(ViewHolder holder, EmbedData item) {
        holder.audioInput.setText(item.getAudio_input());
        holder.timeStart.setText(item.getCreated_at());
        holder.historyStatus.setText(item.getStatus());
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof EmbedData;
    }

    public static class ViewHolder extends ItemViewHolder<EmbedData> {
        @BindView(R.id.history_audio_input)
        TextView audioInput;
        @BindView(R.id.history_time_start)
        TextView timeStart;
        @BindView(R.id.history_status)
        TextView historyStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            setItemClickListener((view1, item) -> click(item));
        }
    }
}
