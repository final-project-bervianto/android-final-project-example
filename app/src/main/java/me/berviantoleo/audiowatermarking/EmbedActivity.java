package me.berviantoleo.audiowatermarking;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.folderselector.FileChooserDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;
import me.berviantoleo.audiowatermarking.api.WebServiceConnector;
import me.berviantoleo.audiowatermarking.model.EmbedData;

public class EmbedActivity extends AppCompatActivity implements FileChooserDialog.FileCallback {

    private StorageReference mStorageReference;
    private FirebaseAuth mAuth;

    @BindView(R.id.audio_input_embed)
    EditText audioPath;

    @BindView(R.id.picture_input_embed)
    EditText picturePath;

    @BindView(R.id.key_embed)
    EditText keyText;

    String audioFix;
    boolean audioReady = false;
    String pictFix;

    boolean pictReady = false;

    private AlertDialog dialog;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_embed);
        ButterKnife.bind(this);
        dialog = new SpotsDialog.Builder().setContext(this).build();
        mAuth = FirebaseAuth.getInstance();
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            runLogin();
        }
    }

    private void runLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.upload_embed)
    void uploadEmbed() {
        String pict = picturePath.getText().toString();
        String audio = audioPath.getText().toString();
        String key = keyText.getText().toString();
        if (pict.equalsIgnoreCase("") && audio.equalsIgnoreCase("")
                && key.equalsIgnoreCase("")) {
            Toast.makeText(this, "Filled All Field", Toast.LENGTH_SHORT).show();
        } else {
            uploadData(pict, audio, key);
        }
    }

    private void uploadData(final String pict, final String audio, final String key) {
        if (!audioReady) {
            dialog.show();
            File audioFile = new File(audio);
            String audioName = audioFile.getName();
            Uri fileAudio = Uri.fromFile(audioFile);
            String target = String.format("%s/embed/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), audioName);
            audioFix = target;
            this.key = key;
            StorageReference audioRef = mStorageReference.child(target);
            audioRef.putFile(fileAudio)
                    .addOnSuccessListener(taskSnapshot -> {
                        audioReady = true;
                        if (!pictReady) {
                            File pictFile = new File(pict);
                            String pictName = pictFile.getName();
                            Uri filePict = Uri.fromFile(pictFile);
                            String targetPict = String.format("%s/embed/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), pictName);
                            pictFix = targetPict;
                            StorageReference pictRef = mStorageReference.child(targetPict);
                            pictRef.putFile(filePict)
                                    .addOnSuccessListener(taskSnapshot1 -> {
                                        dialog.dismiss();
                                        pictReady = true;
                                        uploadToServer();
                                    }).addOnFailureListener(e -> {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            });
                        }
                    }).addOnFailureListener(e -> {
                dialog.hide();
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            });
        } else {
            if (!pictReady) {
                dialog.show();
                File pictFile = new File(pict);
                String pictName = pictFile.getName();
                Uri filePict = Uri.fromFile(pictFile);
                String targetPict = String.format("%s/embed/%s-%s", mAuth.getCurrentUser().getUid(), System.currentTimeMillis(), pictName);
                pictFix = targetPict;
                StorageReference pictRef = mStorageReference.child(targetPict);
                pictRef.putFile(filePict)
                        .addOnSuccessListener(taskSnapshot -> {
                            pictReady = true;
                            dialog.dismiss();
                            uploadToServer();
                        }).addOnFailureListener(e -> {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                });
            }
        }
    }

    private void uploadToServer() {
        dialog.show();
        mAuth.getCurrentUser().getIdToken(true)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String idTok = task.getResult().getToken();
                        SharedPreferences prefs = getSharedPreferences("Audio_Watermarking", MODE_PRIVATE);
                        String restoredText = prefs.getString("Address", "http://bervita.if.itb.ac.id/api/");
                        WebServiceConnector.getInstance(restoredText).postEmbed(audioFix, pictFix, key, idTok,
                                new WebServiceConnector.ApiCallback<EmbedData>() {
                                    @Override
                                    public void onSuccess(EmbedData response) {
                                        dialog.dismiss();
                                        if (response != null) {
                                            Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "FAILED", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.select_audio_embed)
    void selectAudio() {
        new FileChooserDialog.Builder(this)
                .extensionsFilter(".wav") // Optional extension filter, will override mimeType()
                .tag("audio")
                .show(this);
    }

    @OnClick(R.id.select_image_embed)
    void selectImage() {
        new FileChooserDialog.Builder(this)
                .mimeType("image/*") // Optional extension filter, will override mimeType()
                .tag("image")
                .show(this);
    }

    @Override
    public void onFileSelection(@NonNull FileChooserDialog dialog, @NonNull File file) {
        final String tag = dialog.getTag();
        if (tag != null) {
            if (tag.equalsIgnoreCase("audio") || tag.equalsIgnoreCase("image")) {
                String path = file.getPath();
                if (tag.equalsIgnoreCase("audio")) {
                    audioPath.setText(path);
                } else {
                    picturePath.setText(path);
                }
            } else {
                Toast.makeText(this, "Failed Pick", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Failed Pick", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFileChooserDismissed(@NonNull FileChooserDialog dialog) {

    }
}
