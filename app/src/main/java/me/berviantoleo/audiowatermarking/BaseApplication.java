package me.berviantoleo.audiowatermarking;

import androidx.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class BaseApplication extends MultiDexApplication {

    private static BaseApplication instance;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fresco.initialize(this);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.fetch(3600);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
    }
}
