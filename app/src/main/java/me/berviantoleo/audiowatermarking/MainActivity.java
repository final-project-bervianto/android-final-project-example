package me.berviantoleo.audiowatermarking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private TextView name;
    private Button logoutButton;
    private Button historyButton;
    private Button embedButton;
    private Button extractButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        name = findViewById(R.id.name_view);
        logoutButton = findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(view -> logout(view));
        embedButton = findViewById(R.id.embedButton);
        embedButton.setOnClickListener(view -> embed(view));
        extractButton = findViewById(R.id.extractButton);
        extractButton.setOnClickListener(view -> extract(view));
        historyButton = findViewById(R.id.historyButton);
        historyButton.setOnClickListener(view -> history(view));
    }

    void history(View view) {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }

    void extract(View view) {
        Intent intent = new Intent(this, ExtractActivity.class);
        startActivity(intent);
    }

    void embed(View view) {
        Intent intent = new Intent(this, EmbedActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            runLogin();
        } else {
            name.setText(getString(R.string.dashboard_name,user.getEmail()));
        }
    }

    private void runLogin() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    void logout(View view) {
        mAuth.signOut();
        runLogin();
    }

    void selectServer(View view) {
        new MaterialDialog.Builder(this)
                .title(R.string.select_server)
                .items(R.array.server_name_list)
                .itemsCallback((dialog, view1, which, text) -> {
                    String[] address = getResources().getStringArray(R.array.server_list);
                    int select = which;
                    if (select < 0 || select > 2) {
                        select = 0;
                    }
                    String newLoc = address[select];
                    SharedPreferences.Editor editor = getSharedPreferences("Audio_Watermarking", MODE_PRIVATE).edit();
                    editor.putString("Address", newLoc);
                    editor.apply();
                })
                .show();
    }

}
